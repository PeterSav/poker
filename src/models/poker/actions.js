export const INCREMENT_COUNT = "INCREMENT_COUNT";
export const CHANGE_HAND_CARDSTOCHANGE = "CHANGE_HAND_CARDSTOCHANGE";
export const CALCULATE_WINNER = "CALCULATE_WINNER";
export const CALCULATE_WINNER_EPIC = "CALCULATE_WINNER_EPIC";
export const INCREMENT_COUNT_INITIATE = "INCREMENT_COUNT_INITIATE";
export const ON_CLICK_SELECT_EPIC = "ON_CLICK_SELECT_EPIC";


export const calculateWinner = () => {
    return {
        type: CALCULATE_WINNER,
    }
}

export const incrementCountAction = (players, deck, turn) => {
    return {
        type: INCREMENT_COUNT,
        payload: {
            players,
            deck,
            turn
        }
    };
};

export const changeCard = (players) => {
    return {
        type: CHANGE_HAND_CARDSTOCHANGE,
        players
    };
};

export const calculateWinnerEpic = (players) => {
    return {
        type: CALCULATE_WINNER_EPIC,
        players
    }
};

export const incrementCountEpic = (players, deck, turn) => {
    return {
        type: INCREMENT_COUNT_INITIATE,
        players,
        deck,
        turn
    }
}

export const onClickSelectEpic = (players, i, player) => {
    return {
        type: ON_CLICK_SELECT_EPIC,
        players,
        i,
        player
    }
}