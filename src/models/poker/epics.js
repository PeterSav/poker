import { map } from "rxjs/operators";
import { ofType } from 'redux-observable';
import { maxValueIndex, combinations, RateableCards, PokerHandRate, CardsAndDeck } from "../../commons/helpers/helperFunctions";
import { CALCULATE_WINNER_EPIC, CALCULATE_WINNER, INCREMENT_COUNT_INITIATE, INCREMENT_COUNT, ON_CLICK_SELECT_EPIC, CHANGE_HAND_CARDSTOCHANGE } from "./actions";
import zipWith from "lodash/zipWith";
import cloneDeep from "lodash/cloneDeep"

export const calculateWinnerEpic = action$ => action$.pipe(
    ofType(CALCULATE_WINNER_EPIC),
    map(action => {
        const playersValue = action.players.map(player => player.combination = combinations.indexOf(player.combination));
        const winner = maxValueIndex(playersValue, 0);
        return ({
            type: CALCULATE_WINNER,
            winner: action.players[winner].name
        })
    })
);

export const incrementCountEpic = action$ => action$.pipe(
    ofType(INCREMENT_COUNT_INITIATE),
    map(action => {
        const numberOfCardsToChange = () => {
            return action.players[1].CardsToChange.filter(isSelected => isSelected).length
        };
        const { cards, deck2 } = CardsAndDeck(action.deck, numberOfCardsToChange());
        const [host, player] = action.players;
        const newPlayers = [
            host,
            {
                ...player,
                hand: zipWith(
                    player.hand,
                    player.CardsToChange,
                    (playerCard, cardToChange) => cardToChange ? cards.pop() : playerCard
                ),
                CardsToChange: [false, false, false, false, false],
            }
        ];
        newPlayers.forEach(player => {
            player.combination = PokerHandRate(new RateableCards(player.hand))
        })
        return ({
            type: INCREMENT_COUNT,
            payload: {
                players: newPlayers,
                turn: action.turn + 1,
                deck: deck2
            }
        })
    })
);

export const onClickSelectEpic = action$ => action$.pipe(
    ofType(ON_CLICK_SELECT_EPIC),
    map(action => {
        const playerCardSelected = () => {
            const playa = action.players.find(pl => pl.name === action.player);
            return playa.CardsToChange;
        }
        const newSelected = playerCardSelected();
        newSelected[action.i] = !newSelected[action.i];
        const newPlayers = cloneDeep(action.players);
        newPlayers[1].CardsToChange = newSelected;
        return ({
            type: CHANGE_HAND_CARDSTOCHANGE,
            players: newPlayers
        })
    })
)