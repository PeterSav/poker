import { CardsAndDeck, Cards, players } from "../../commons/helpers/helperFunctions";
import { CHANGE_HAND_CARDSTOCHANGE, INCREMENT_COUNT, CALCULATE_WINNER } from "./actions";
import { combineEpics } from "redux-observable";
import { calculateWinnerEpic, incrementCountEpic, onClickSelectEpic } from "./epics"

const initialState = players.reduce(
    ({ players, deck: currentDeck, ...rest }, player) => {
        const { cards, deck } = CardsAndDeck(currentDeck, 5);
        return {
            ...rest,
            players: [
                ...players,
                {
                    name: player,
                    hand: cards,
                    CardsToChange: [false, false, false, false, false],
                    combination: "",
                },
            ],
            deck,
        };
    },
    {
        players: [],
        deck: Cards,
        turn: 0,
        winner: "",
    }
);

export const rootEpic = combineEpics(
    calculateWinnerEpic,
    incrementCountEpic,
    onClickSelectEpic
);

export const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_HAND_CARDSTOCHANGE:
            return {
                ...state,
                players: action.players,
            }
        case INCREMENT_COUNT:
            return {
                ...state,
                players: action.payload.players,
                deck: action.payload.deck,
                turn: action.payload.turn,
            }
        case CALCULATE_WINNER:
            return {
                ...state,
                winner: action.winner
            }
        default:
            return state;
    }
};

