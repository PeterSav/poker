import _ from 'lodash';

function deepFreeze(object) {
    if (typeof object !== 'object') {
        return object;
    }
    Object.freeze(object);

    Object.values(object)
        .forEach(value => deepFreeze(value));

    return object;
}

const maxInARow = weights =>
    _.chain(weights)
        .sortBy()
        .uniq()
        .map((num, i) => num - i)
        .groupBy()
        .orderBy('length')
        .last()
        .value()
        .length;

export class RateableCards {
    constructor(cards) {
        this.ranks = _.groupBy(cards, 'rank');
        this.suits = _.groupBy(cards, 'suit');
        this.rankTimes = _.groupBy(this.ranks, 'length');
        this.suitTimes = _.groupBy(this.suits, 'length');
        this.maxInARow = maxInARow(cards.map(({ weight }) => weight));
    }

    getOfSameRank(n) { return this.rankTimes[n] || []; }

    getOfSameSuit(n) { return this.suitTimes[n] || []; }

    hasAce() { return !!this.ranks['A']; }

    hasOfSameRank(n) { return this.getOfSameRank(n).length; }

    hasOfSameSuit(n) { return this.getOfSameSuit(n).length; }

    hasInARow(n) { return this.maxInARow >= n; }

    getWorstSingles() {
        return _.chain(this.getOfSameRank(1))
            .flatten()
            .sortBy('weight')
            .value();
    }
}

//
// Poker Ratings
//
export const PokerRating = {
    RoyalFlush: hand => hand.hasInARow(5) && hand.hasOfSameSuit(5) && hand.hasAce(),
    StraightFlush: hand => hand.hasInARow(5) && hand.hasOfSameSuit(5),
    FourOfAKind: hand => hand.hasOfSameRank(4),
    FullHouse: hand => hand.hasOfSameRank(3) && hand.hasOfSameRank(2),
    Flush: hand => hand.hasOfSameSuit(5),
    Straight: hand => hand.hasInARow(5),
    ThreeOfAKind: hand => hand.hasOfSameRank(3),
    TwoPair: hand => hand.hasOfSameRank(2) >= 2,
    OnePair: hand => hand.hasOfSameRank(2),
    HighCard: hand => hand.hasOfSameRank(1) >= 5,
};


const Ranks = Object.freeze(['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']);
const Suits = Object.freeze(['hearts', 'clubs', 'diams', 'spades']);

export const Cards = deepFreeze(
    Object.entries(Ranks).reduce(
        (cards, [weight, rank]) =>
            [...cards, ...Suits.map(suit => ({ rank, suit, weight }))],
        []
    )
);


export const CardsAndDeck = (currentDeck = Cards, n = 0) => {
    const deck = currentDeck !== Cards
        ? currentDeck.slice(n, currentDeck.length)
        : currentDeck.slice(n, currentDeck.length).sort(() => Math.random() - 0.5);

    Object.freeze(deck);

    const cards1 = Object.freeze(currentDeck.slice(0, n));
    const cards = cards1.map(card => {
        const cardToReturn = { ...card, weight: parseInt(card.weight) }
        return cardToReturn;
    })
    return {
        cards,
        deck,
    };
};

const [H, C, D, S] = Suits;
const c = (weight, suit) => ({ rank: Ranks[weight], suit, weight });

export const hands = [
    [c(12, H), c(8, H), c(12, C), c(8, C), c(7, S), c(12, D)],
    [c(12, H), c(12, D), c(12, C), c(8, C), c(12, S), c(6, D)],
    [c(12, H), c(8, H), c(11, H), c(10, H), c(9, H), c(9, C)],
    [c(12, H), c(8, H), c(12, C), c(6, C), c(7, S), c(7, D)],
    [c(12, H), c(8, H), c(7, C), c(3, C), c(5, S), c(4, D)],
]


export const maxValueIndex = (arr, valueOfMax) => {
    return arr.reduce((acc, element, index) => {
        if (valueOfMax > element) {
            acc = index;
            valueOfMax = element;
        }
        return acc;
    }, 0);
};

export const combinations = ["RoyalFlush", "StraightFlush", "FourOfAKind", "FullHouse", "Flush", "Straight", "ThreeOfAKind", "TwoPair", "OnePair", "HighCard"]

export const players = ["host", "player"];

export const PokerHandRate = cards => Object.entries(PokerRating).find(([, is]) => is(cards))[0];
