import "./poker.scss";
import React from "react";
import Hand from "./Hand";
import { connect } from "react-redux";
import { calculateWinnerEpic, incrementCountEpic } from "../../models/poker/actions";

const Poker = ({ dispatch, players, turn, deck, winner }) => {

    const incrementCount = () => {
        dispatch(incrementCountEpic(players, deck, turn));
    }

    const calculateTheWinner = () => {
        dispatch(calculateWinnerEpic(players))
    }

    const cardsCanBeSelected = () => !turn;

    const createHand = player => {
        return (
            <div className={player.name === "player" ? "playerHand" : ""} key={player.name}>
                <Hand
                    hand={player.hand}
                    player={player.name}
                    cardsCanBeSelected={cardsCanBeSelected()}
                />
            </div>
        )
    }

    return (
        <div className="app">
            <button className="changeCards glow-on-hover" onClick={incrementCount}>
                CHANGE CARDS
            </button>
            <button className="calculateWinner glow-on-hover" onClick={calculateTheWinner} disabled={turn ? false : true}>
                {turn ? "END GAME" : "disabled"}
            </button>
            <div className="mainContainer">
                {winner && (
                    <div className="winner">
                        The winner is {winner}!
                    </div>
                )}
                {players.map(player => createHand(player))}
            </div>
        </div>
    )
};


const mapStateToProps = state => {
    return {
        players: state.players,
        turn: state.turn,
        deck: state.deck,
        winner: state.winner,
    }
}

export default connect(mapStateToProps)(Poker);