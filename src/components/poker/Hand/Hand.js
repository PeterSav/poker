import "./Hand.scss";
import React from "react";
import Card from "./Card";
import { connect } from "react-redux";
import { onClickSelectEpic } from "../../../models/poker/actions"

const Hand = ({ hand, player, players, dispatch, cardsCanBeSelected }) => {

    const onClickSelect = i => () => {
        dispatch(onClickSelectEpic(players, i, player))
    }

    const playerCardSelected = () => {
        const playa = players.find(pl => pl.name === player);
        return playa.CardsToChange;
    }

    const isFront = () => (player === "player");

    const isSelectable = () => (isFront() && cardsCanBeSelected);

    return (
        <div className="playingCards simpleCards">
            {hand.map((hand, id) => (
                <Card
                    onClickSelect={onClickSelect(id)}
                    isSelected={playerCardSelected()[id]}
                    key={hand.rank + hand.suit}
                    rank={hand.rank}
                    suit={hand.suit}
                    isSelectable={isSelectable()}
                    isFront={isFront()}
                />
            ))}
        </div>
    )
}

const mapStateToProps = state => {
    return {
        players: state.players,
    }
}

export default connect(mapStateToProps)(Hand);