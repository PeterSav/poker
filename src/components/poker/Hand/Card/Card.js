import "./Card.scss";
import React from "react";

const Card = ({ rank, suit, onClickSelect, isSelected, isFront, isSelectable }) => {
    let cardSelectedClass = isSelected && isSelectable ? "selected" : "notSelected";

    if (isFront) {
        let card = `card rank-${rank} ${suit} `;
        return (
            <div className={card + cardSelectedClass} onClick={onClickSelect}>
                <span className="rank">{rank}</span>
                <span className="suit">{suit}</span>
            </div>
        )
    }
    else {
        return (
            <div className="card back">*</div>
        )
    }
}

export default Card;