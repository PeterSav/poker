import React from 'react';
import './App.css';
import Poker from "./components/poker";
import { createStore, applyMiddleware } from "redux";
import rootReducer from "./models/poker/index";
import { Provider } from "react-redux";
import { rootEpic } from "./models/poker/reducers";
import { createEpicMiddleware } from "redux-observable";

const epicMiddleware = createEpicMiddleware();

const store = createStore(
  rootReducer,
  applyMiddleware(epicMiddleware)
);

epicMiddleware.run(rootEpic);

function App() {
  return (
    <Provider store={store}>
      <Poker />
    </Provider>
  );
}

export default App;
